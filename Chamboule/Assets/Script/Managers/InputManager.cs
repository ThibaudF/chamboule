﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Trigger PlayerDraggedEvent each time the player clicked
    public PlayerDraggedEvent PlayerDraggedEvent;
    // Trigger PLayerClickedEvent each FixedUpdate while the player is dragging.
    // Param : the movement done between two FixedUpdate
    public PlayerClickedEvent PlayerClickedEvent;

    private Vector3 _dragStartPosition;
    private bool _isDragging = false;

    public void OnClick()
    {
        if (!_isDragging)
            PlayerClickedEvent.Trigger(Input.mousePosition);
    }

    public void OnBeginDrag()
    {
        _isDragging = true;
        _dragStartPosition = Input.mousePosition;
    }

    public void OnEndDrag()
    {
        _isDragging = false;
    }

    public void FixedUpdate()
    {
        if (_isDragging)
        {
            Vector2 dir = (Input.mousePosition - _dragStartPosition);
            Vector2 dirPower = new Vector2(dir.x / Screen.width, dir.y / Screen.height);
            PlayerDraggedEvent.Trigger(dirPower);
            _dragStartPosition = Input.mousePosition;
        }
    }
}
