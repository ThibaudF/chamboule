﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStatsInfos : MonoBehaviour
{
    public Text BulletsLeft;
    public GameManager GameManager;
    public Text CurrentLevel;
    public Text NextLevel;
    public Image CurrentLevelProgress;

    private void Update()
    {
        string shotsLeft = GameManager.GameDatas.ShotsLeft.ToString();
        if (BulletsLeft.text != shotsLeft)
            BulletsLeft.text = shotsLeft;
        string currentLevel = (LevelManager.Instance.CurrentLevel + 1).ToString();
        if (CurrentLevel.text != currentLevel)
            CurrentLevel.text = currentLevel;
        string nextLevel = (LevelManager.Instance.CurrentLevel + 2).ToString();
        if (NextLevel.text != nextLevel)
            NextLevel.text = nextLevel;

        float FloorsNeeded = GameManager.GameDatas.LevelInfos.FloorsQuantity - GameManager.Settings.FloorsLeftToEnd;
        float FloorsDestroyed = FloorsNeeded - (GameManager.GameDatas.FloorsLeft - GameManager.Settings.FloorsLeftToEnd);
        float completionPercent = FloorsDestroyed / FloorsNeeded;
        CurrentLevelProgress.fillAmount = completionPercent;
    }
}
