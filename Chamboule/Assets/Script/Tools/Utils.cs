﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{

    /// <summary>
    /// Rotates one point around another
    /// </summary>
    /// <param name="pointToRotate">The point to rotate.</param>
    /// <param name="centerPoint">The center point of rotation.</param>
    /// <param name="angleInDegrees">The rotation angle in degrees.</param>
    /// <returns>Rotated point</returns>
    public static Vector3 RotatePoint(Vector2 pointToRotate, Vector2 centerPoint, double angleInRadians, bool computeCosAndSin = true, double cosTheta = 0.0, double sinTheta = 0.0)
    {
        if (computeCosAndSin)
        {
            cosTheta = Math.Cos(angleInRadians);
            sinTheta = Math.Sin(angleInRadians);
        }
        return new Vector2((float)(cosTheta * (pointToRotate.x - centerPoint.x) - sinTheta * (pointToRotate.y - centerPoint.y) + centerPoint.x),
                           (float)(sinTheta * (pointToRotate.x - centerPoint.x) + cosTheta * (pointToRotate.y - centerPoint.y) + centerPoint.y));
    }

    /// <summary>
    /// Return the first active layer from a LayerMask
    /// </summary>
    /// <param name="layerMask">The LayerMask to check.</param>
    /// <returns>a single layer index</returns>
    public static int layermask_to_layer(LayerMask layerMask)
    {
        int layerNumber = 0;
        int layer = layerMask.value;
        while (layer > 0)
        {
            layer = layer >> 1;
            layerNumber++;
        }
        return layerNumber - 1;
    }

    /// <summary>
    /// return a new list with a random composition of 'quantity' elements from 'array'
    /// </summary>
    /// <param name="array">The array from which elements will be picked.</param>
    /// <param name="quantity">The quantity of elements to pick.</param>
    /// <param name="unique">unique : should we pick each elements only once, or not ?.</param>
    /// <returns>a list with the desired elements</returns>
    public static List<T> SelectRandomElements<T>(T[] array, int quantity, bool unique = true)
    {
        return SelectRandomElements<T>(new List<T>(array), quantity, unique);
    }

    /// <summary>
    /// return a new list with a random composition of 'quantity' elements from 'array'
    /// </summary>
    /// <param name="array">The array from which elements will be picked.</param>
    /// <param name="quantity">The quantity of elements to pick.</param>
    /// <param name="unique">unique : should we pick each elements only once, or not ?.</param>
    /// <returns>a list with the desired elements</returns>
    public static List<T> SelectRandomElements<T>(List<T> array, int quantity, bool unique = true)
    {
        List<T> res = new List<T>();
        if (array.Count == 0)
            return res;

        if (unique && quantity >= array.Count)
        {
            res.AddRange(array);
        }
        else
        {
            List<T> l = new List<T>(array);

            while (res.Count != quantity)
            {
                int rand = UnityEngine.Random.Range(0, l.Count);
                T elem = l[rand];
                res.Add(elem);

                if (unique)
                    l.RemoveAt(rand);
            }
        }

        return res;
    }
}
