﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatMenu : MonoBehaviour
{
    /*
     * Cheats : 
     * R = Reload menu
     * Right = increase level
     * Up = FastForward Increase Level
     * Left = decrease level
     * Down = FastForward Decrease Level
     * U = unfreeze
     **/

#if DEBUG
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            LevelManager.Instance.LoadCurrentLevel();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            LevelManager.Instance.IncrementLevel(1);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            LevelManager.Instance.IncrementLevel(-1);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            LevelManager.Instance.IncrementLevel(10);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            LevelManager.Instance.IncrementLevel(-10);
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            FindObjectOfType<ElementCreator>().UnFreeze();
        }

    }
#endif
}
