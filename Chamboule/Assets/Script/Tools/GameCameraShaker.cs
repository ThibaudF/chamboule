﻿using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCameraShaker : MonoBehaviour
{
    public ScreenShakeConfig Config;

    // Active shake
    class ActiveShake
    {
        // Shake configuration
        public ScreenShakeConfig config;
        // Time elapsed in shake
        public float Time;
        // Random seed for the shake
        public Vector3 Seeds;
    }

    // Initial position of the camera
    private Vector3 _initialPosition;

    // All active shakes
    List<ActiveShake> _activeShakes = new List<ActiveShake>(8);

    // On awake
    private void Awake()
    {
        _initialPosition = transform.localPosition;
        Config.Event += Shake;
    }

    private void OnDestroy()
    {
        Config.Event -= Shake;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 delta = Vector3.zero;
        int i = 0;
        while (i < _activeShakes.Count)
        {
            ActiveShake shake = _activeShakes[i];
            if (shake.Time < shake.config.InDuration + shake.config.OutDuration)
            {
                shake.Time += Time.deltaTime;
                float ease = (shake.Time < shake.config.InDuration) ?
                    EaseManager.Evaluate(shake.config.InEase, null, shake.Time, shake.config.InDuration, 0, 0) :
                    1.0f - EaseManager.Evaluate(shake.config.OutEase, null, shake.Time - shake.config.InDuration, shake.config.OutDuration, 0, 0);
                float vibrato = shake.config.Vibrato;
                float strength = ease * shake.config.Strength;
                for (int o = 1; o <= shake.config.Harmonics + 1; ++o)
                {
                    float noiseTime = shake.Time * vibrato;
                    delta += new Vector3(
                        (1.0f - 2.0f * Mathf.PerlinNoise(shake.Seeds.x * o, noiseTime)) * strength,
                        (1.0f - 2.0f * Mathf.PerlinNoise(shake.Seeds.y * o, noiseTime)) * strength,
                        (1.0f - 2.0f * Mathf.PerlinNoise(shake.Seeds.z * o, noiseTime)) * strength);
                    vibrato *= 2.0f;
                    strength *= shake.config.HarmonicsAttenuation;
                }
                ++i;
            }
            else
            {
                _activeShakes.RemoveAt(i);
            }
        }
        transform.localPosition = _initialPosition + delta;
    }

    // Start a shake of the camera
    public void Shake(ScreenShakeConfig shake)
    {
        _activeShakes.Add(new ActiveShake()
        {
            Time = 0,
            config = shake,
            Seeds = new Vector3(Random.Range(0.0f, 10000.0f), Random.Range(0.0f, 10000.0f), Random.Range(0.0f, 10000.0f))
        });
    }

    // Reset the shake
    public void Reset()
    {
    }
}
