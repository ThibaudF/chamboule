﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScreenShakeConfig", menuName = "Config/ScreenShakeConfig")]
public class ScreenShakeConfig : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler(ScreenShakeConfig config);

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger()
    {
        if (Event != null)
        {
            Event(this);
        }
    }

    [Header("Shake In")]
    [Tooltip("The duration of the shake")]
    public float InDuration = 0.03333334f;
    [Tooltip("Ease")]
    public Ease InEase = Ease.Linear;
    [Header("Shake Out")]
    [Tooltip("The duration of the shake")]
    public float OutDuration;
    [Tooltip("Ease")]
    public Ease OutEase = Ease.Linear;

    [Header("Common")]
    [Tooltip("The shake strength")]
    public float Strength;
    [Tooltip("Frequency of the shake")]
    public int Vibrato = 10;
    [Tooltip("Number of additional harmonics.")]
    public int Harmonics = 1;
    [Tooltip("Attanuation of an harmonic.")]
    public float HarmonicsAttenuation = 0.5f;
}
