﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Event triggered each time the player click on the screen
[CreateAssetMenu(fileName = "PlayerClickedEvent", menuName = "Event/PlayerClickedEvent")]
public class PlayerClickedEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler(Vector2 screenPosition);

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger(Vector2 screenPosition)
    {
        if (Event != null)
        {
            Event(screenPosition);
        }
    }
}
