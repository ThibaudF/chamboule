﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Event triggered each time the player clicked, but not on an active Cylinder
[CreateAssetMenu(fileName = "EmptyClickedEvent", menuName = "Event/EmptyClickedEvent")]
public class EmptyClickedEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler();

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger()
    {
        if (Event != null)
        {
            Event();
        }
    }
}
