﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A simple injectable toogle event
[CreateAssetMenu(fileName = "SimpleEvent", menuName = "Event/Simple Event")]
public class SimpleEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler();

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger()
    {
        if (Event != null)
        {
            Event();
        }
    }
}
