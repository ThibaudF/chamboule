﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// event triggered each time the player click on a cylinder
[CreateAssetMenu(fileName = "ItemClickedEvent", menuName = "Event/ItemClickedEvent")]
public class ItemClickedEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler(CylinderElement element, Vector3 hitPoint);

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger(CylinderElement element, Vector3 hitPoint)
    {
        if (Event != null)
        {
            Event(element, hitPoint);
        }
    }
}
