﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Event triggered each time the last item of a floor disappear
[CreateAssetMenu(fileName = "FloorDisappearEvent", menuName = "Event/FloorDisappearEvent")]
public class FloorDisappearEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler();

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger()
    {
        if (Event != null)
        {
            Event();
        }
    }
}
