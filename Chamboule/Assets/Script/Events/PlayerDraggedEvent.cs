﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Event triggered each FixedUpdate of dragging from the player
[CreateAssetMenu(fileName = "PlayerDraggedEvent", menuName = "Event/PlayerDraggedEvent")]
public class PlayerDraggedEvent : ScriptableObject
{
    // handler for a Toogle event
    public delegate void Handler(Vector2 dragDirection);

    // Event
    public event Handler Event;

    // Trigger the event
    public void Trigger(Vector2 dragDirection)
    {
        if (Event != null)
        {
            Event(dragDirection);
        }
    }
}
