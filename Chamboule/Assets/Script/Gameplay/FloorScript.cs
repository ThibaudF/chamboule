﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        CylinderElement element = collision.collider.GetComponent<CylinderElement>();
        if (element != null)
        {
            element.TriggerFloorHit();
        }
    }
}
