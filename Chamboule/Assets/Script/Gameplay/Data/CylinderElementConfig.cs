﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CylinderElementConfig", menuName = "Config/CylinderElementConfig")]
public class CylinderElementConfig : ScriptableObject
{
    [Tooltip("Color material used when the item is active")]
    public Material AvailableMaterial;
    [Tooltip("Unique ID used to identify a color and spread destruction")]
    public int ElementTypeID;
}
