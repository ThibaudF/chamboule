﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SingleLevelDesign))]
public class SingleLevelDesignEditor : Editor
{
    private string backendURL;

    public override void OnInspectorGUI()
    {
        SingleLevelDesign levelDesign = target as SingleLevelDesign;

        GUI.changed = false;

        GUILayout.Label("level number", EditorStyles.boldLabel);
        levelDesign.Level = EditorGUILayout.IntField(levelDesign.Level);
        levelDesign.LevelInfos.MinLevel = levelDesign.Level;

        GUILayout.Label("Color quantity", EditorStyles.boldLabel);
        GUILayout.Label(levelDesign.LevelInfos.ColorQuantity.ToString());

        GUILayout.Label("Floor quantity", EditorStyles.boldLabel);
        GUILayout.Label(levelDesign.LevelInfos.FloorsQuantity.ToString());

        GUILayout.Label("Shot quantity", EditorStyles.boldLabel);
        levelDesign.LevelInfos.ShotQuantity = EditorGUILayout.IntField(levelDesign.LevelInfos.ShotQuantity);

        GUILayout.Label("Layout (Use 0123456789 for elements)", EditorStyles.boldLabel);
        levelDesign.StringToItems(EditorGUILayout.TextArea(levelDesign.ItemsToString()));
        
        if (GUI.changed)
        {
            EditorUtility.SetDirty(levelDesign);
        }
    }

    private static bool ToggleFiled(string titile, bool value)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(titile);
        EditorGUILayout.EndHorizontal();

        return value;
    }

}
