﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Config/GameSettings")]
public class GameSettings : ScriptableObject
{
    [Header("Default gameplay")]
    [Tooltip("Quantity of cylinders per floor. Will influence the size of each cylinder.")]
    public int ItemQuantity;
    [Tooltip("Size of the cylinders on each floor")]
    public float FloorHeight;
    [Tooltip("Size of the Bloc supporting all the cylinders")]
    public float BaseHeight;
    [Tooltip("Quantity of rows visible by the camera")]
    public int RowVisibleOnScreen;
    [Tooltip("Quantity of rows breakable")]
    public int AccessibleFloors;
    [Tooltip("if they are less than 'FloorsLeftToEnd' floors left, the game end as a victory")]
    public int FloorsLeftToEnd;
    [Tooltip("if true, bullets will only be able to hit cylinders of the same color")]
    public bool UseColorForBullet;
    [Tooltip("Force applied to any adjacent item when a cylinder is destroyed")]
    public float ExplosionStrength;
    [Tooltip("Array of available cylinders style")]
    public CylinderElement[] DefaultCylinders;
    [Header("Levels datas")]
    [Tooltip("Array of level configs.")]
    public LevelInfos[] LevelInfos;
    [Tooltip("Array of specific levels configs.")]
    public SingleLevelDesign[] SpecificLevels;
    [Header("Heavy Physics Dependant Config")]
    [Tooltip("Items are disposed in circle on the base, forming a circle of 'DistanceFromCenter' radius")]
    public float DistanceFromCenter;
    [Tooltip("between 0 - 1, offset for the origin of the circle each floor. At 0.5, each cylinder will be on top of the middle of two cylinders of the previous floor")]
    public float OffsetPerFloor;
}

[Serializable]
public class LevelInfos
{
    [Tooltip("This config will be used between this MinLevel and the next array element 'MinLevel'.")]
    public int MinLevel;

    [Tooltip("Quantity of floors.")]
    public int FloorsQuantity;
    [Tooltip("Quantity of possible colors at the same time.")]
    public int ColorQuantity;
    [Tooltip("Quantity of shot allowed for the player before game over.")]
    public int ShotQuantity;
}
