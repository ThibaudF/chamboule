﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDatas
{
    // Current level configuration
    public LevelInfos LevelInfos;
    // Current player shots left before game over
    public int ShotsLeft;
    // Current rows already fallen/destroyed
    public int RowDone;

    public int FloorsLeft
    {
        get
        {
            return LevelInfos.FloorsQuantity - RowDone;
        }
    }

    public GameDatas(LevelInfos level)
    {
        LevelInfos = level;
        ShotsLeft = level.ShotQuantity;
        RowDone = 0;
    }
}
