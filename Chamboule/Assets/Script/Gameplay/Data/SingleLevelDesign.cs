﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "SingleLevelDesign", menuName = "Config/SingleLevelDesign")]
public class SingleLevelDesign : ScriptableObject
{
    public int Level;
    [SerializeField]
    public List<LineOfLevel> Items = new List<LineOfLevel>();
    public LevelInfos LevelInfos = new LevelInfos();

    public string ItemsToString()
    {
        StringBuilder sb = new StringBuilder();
        foreach (LineOfLevel l in Items)
        {
            foreach (int i in l.Line)
            {
                sb.Append(i);
            }
            sb.AppendLine();
        }
        return sb.ToString();
    }

    public void StringToItems(string items)
    {
        string[] splits = items.Split('\n');
        int maxElem = 0;
        int maxColor = 0;
        List<LineOfLevel> newList = new List<LineOfLevel>();
        foreach (string line in splits)
        {
            List<int> newLine = new List<int>();
            foreach (char c in line)
            {
                if (c < '0' || c > '9')
                    continue;

                int color = c - '0';
                newLine.Add(color);
                maxColor = Mathf.Max(color, maxColor);
            }
            if (newLine.Count != 0)
            {
                newList.Add(new LineOfLevel(newLine));
                maxElem = Mathf.Max(newLine.Count, maxElem);
            }
        }

        foreach (LineOfLevel line in newList)
        {
            while (line.Line.Count < maxElem)
                line.Line.Add(0);
        }

        if (newList.Count != 0 && maxElem != 0)
        {
            Items = newList;
            LevelInfos.ColorQuantity = maxColor + 1;
            LevelInfos.FloorsQuantity = Items.Count;
        }
    }
}

[Serializable]
public class LineOfLevel
{
    public List<int> Line;

    public LineOfLevel(List<int> line)
    {
        Line = line;
    }
}
