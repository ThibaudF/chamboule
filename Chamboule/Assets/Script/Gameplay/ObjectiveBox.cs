﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveBox : MonoBehaviour
{
    public FloorDisappearEvent OnFloorDisappearEvent;

    private bool _hasCollision = true;

    public void OnFloorDisappear()
    {
        OnFloorDisappearEvent.Trigger();
    }

    private void FixedUpdate()
    {
        if (!_hasCollision)
            OnFloorDisappear();
        _hasCollision = false;
    }

    private void OnTriggerStay(Collider other)
    {
        _hasCollision = true;
    }
}
