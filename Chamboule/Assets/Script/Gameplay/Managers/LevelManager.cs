﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager
{
    #region Singleton
    private static LevelManager _instance;
    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = new LevelManager();

            return _instance;
        }
    }
    #endregion

    private int _currentLevel = 0;
    public int CurrentLevel { get { return _currentLevel; } }

    public void LoadNextLevel()
    {
        _currentLevel++;
        LoadCurrentLevel();
    }

    public void GoToLevel(int level)
    {
        if (level < 0)
            level = 0;
        _currentLevel = level;
        LoadCurrentLevel();
    }

    // For now, the scene is reloaded for each new level
    public void LoadCurrentLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

#if DEBUG
    public void IncrementLevel(int qty)
    {
        _currentLevel += qty;
        if (_currentLevel < 0)
            _currentLevel = 0;
    }
#endif
}
