﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera Camera;
    public Vector3 CurrentCenter;
    public PlayerDraggedEvent PlayerDraggedEvent;
    public float DragPower;
    public float SlowPower;
    public float StopPower;
    public float MaxPower;
    public float YOffset;
    public float YMaxScrollPerUpdate;

    private float _currentMomentum;

    public void ForcecameraToTarget()
    {
        Camera.transform.position = new Vector3(Camera.transform.position.x, CurrentCenter.y - YOffset, Camera.transform.position.z);
        Vector3 CenterLookat = new Vector3(CurrentCenter.x, Camera.transform.position.y + YOffset, CurrentCenter.z);
        Camera.transform.LookAt(CenterLookat);
    }

    private void FixedUpdate()
    {
        if (_currentMomentum != 0)
        {
            Vector2 Center2D = new Vector2(CurrentCenter.x, CurrentCenter.z);
            Vector2 Pos2D = new Vector2(Camera.transform.position.x, Camera.transform.position.z);
            Vector2 newPos = Utils.RotatePoint(Pos2D, Center2D, _currentMomentum);
            Camera.transform.position = new Vector3(newPos.x, Camera.transform.position.y, newPos.y);
            _currentMomentum *= SlowPower;
            if (_currentMomentum <= StopPower && _currentMomentum >= -StopPower)
                _currentMomentum = 0;
        }
        MoveCameraToTarget();
    }

    private void MoveCameraToTarget()
    {
        float YDest = CurrentCenter.y - YOffset;
        float currentPosY = Camera.transform.position.y;
        float power = YMaxScrollPerUpdate / Time.timeScale;

        if (YDest > currentPosY + power)
        {
            Camera.transform.position = new Vector3(Camera.transform.position.x, currentPosY + power, Camera.transform.position.z);
        }
        else if (YDest < currentPosY - power)
        {
            Camera.transform.position = new Vector3(Camera.transform.position.x, currentPosY - power, Camera.transform.position.z);
        }
        else
        {
            Camera.transform.position = new Vector3(Camera.transform.position.x, YDest, Camera.transform.position.z);
        }
        Vector3 CenterLookat = new Vector3(CurrentCenter.x, Camera.transform.position.y + YOffset, CurrentCenter.z);
        Camera.transform.LookAt(CenterLookat);
    }

    private void Awake()
    {
        PlayerDraggedEvent.Event += OnPlayerDragged;
        if (Camera == null)
            Camera = GetComponent<Camera>();
    }

    private void OnDestroy()
    {
        PlayerDraggedEvent.Event -= OnPlayerDragged;
    }

    private void OnPlayerDragged(Vector2 dir)
    {
        _currentMomentum += -dir.x * DragPower;
        if (_currentMomentum > MaxPower)
            _currentMomentum = MaxPower;
        if (_currentMomentum < -MaxPower)
            _currentMomentum = -MaxPower;
    }

}
