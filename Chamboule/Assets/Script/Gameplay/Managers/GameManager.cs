﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public const float BASE_SCALING = 1.25f;

    public FloorDisappearEvent FloorDisappearEvent;
    public ItemClickedEvent ItemClickedEvent;
    public EmptyClickedEvent EmptyClickedEvent;
    public GameSettings Settings;
    public ElementCreator ElementCreator;
    public GameObject Base;
    public BoxCollider ObjectiveBox;
    public CameraManager Camera;
    public BulletLauncher BulletLauncher;
    public Vector3 Origin;
    public float CameraYOffset;
    public float FreezeInitialTimer;
    public float FreezeRepeatTimer;

    private Tween _freezeEndTween;

    private GameDatas _gameDatas;
    public GameDatas GameDatas{ get{ return _gameDatas; } }

    private void Awake()
    {
        FloorDisappearEvent.Event += OnFloorDisappear;
        ItemClickedEvent.Event += OnItemClicked;
        EmptyClickedEvent.Event += OnEmptyClicked;
    }

    private void OnDestroy()
    {
        FloorDisappearEvent.Event -= OnFloorDisappear;
        ItemClickedEvent.Event -= OnItemClicked;
        EmptyClickedEvent.Event -= OnEmptyClicked;
        if (_freezeEndTween != null)
            _freezeEndTween.Kill();
    }

    private void Start()
    {
        SingleLevelDesign levelDesign = GetCurrentLevelDesign();
        LevelInfos currentLevel;
        if (levelDesign == null)
            currentLevel = GetCurrentLevelInfos();
        else
            currentLevel = levelDesign.LevelInfos;
        _gameDatas = new GameDatas(currentLevel);
        float baseSize = 2f * Settings.DistanceFromCenter * BASE_SCALING;
        Base.transform.localScale = new Vector3(baseSize, Settings.BaseHeight, baseSize);
        ObjectiveBox.size = new Vector3(baseSize, ObjectiveBox.size.y, baseSize);
        ObjectiveBox.transform.position = Origin + Vector3.up * (_gameDatas.LevelInfos.FloorsQuantity * Settings.FloorHeight * 2f + Settings.BaseHeight);
        Camera.CurrentCenter = ObjectiveBox.transform.position - Vector3.up * CameraYOffset;
        Camera.ForcecameraToTarget();

        if (levelDesign == null)
            ElementCreator.Init(Origin, Settings.BaseHeight, _gameDatas.LevelInfos);
        else
            ElementCreator.Init(Origin, Settings.BaseHeight, levelDesign);

        if (Settings.UseColorForBullet)
            BulletLauncher.ShowNextBullet(ElementCreator.GetRandomBulletConfig());
        else
            BulletLauncher.ShowNextBullet(null);
    }

    private LevelInfos GetCurrentLevelInfos()
    {
        int currentLevel = LevelManager.Instance.CurrentLevel;
        LevelInfos res = null;
        foreach (LevelInfos level in Settings.LevelInfos)
        {
            if (level.MinLevel <= currentLevel)
                res = level;
            else
                break;
        }

        return res;
    }

    private SingleLevelDesign GetCurrentLevelDesign()
    {
        int currentLevel = LevelManager.Instance.CurrentLevel;
        foreach (SingleLevelDesign level in Settings.SpecificLevels)
        {
            if (level.Level == currentLevel)
                return level;
        }

        return null;
    }

    private void OnFloorDisappear()
    {
        _gameDatas.RowDone++;
        ObjectiveBox.transform.position -= Vector3.up * Settings.FloorHeight * 2f;
        if (_gameDatas.FloorsLeft >= Settings.RowVisibleOnScreen)
        {
            Camera.CurrentCenter = ObjectiveBox.transform.position - Vector3.up * CameraYOffset;
        }

        if (_gameDatas.FloorsLeft <= Settings.FloorsLeftToEnd)
        {
            LevelManager.Instance.LoadNextLevel();
        }
    }

    private void FireBullet(CylinderElement element, Vector3 hitPoint)
    {
        _gameDatas.ShotsLeft--;
        int elementID = -1;
        if (Settings.UseColorForBullet)
            elementID = BulletLauncher.NextBulletConfig.ElementTypeID;

        Tween hitTween = DOVirtual.DelayedCall(BulletLauncher.BulletPathDuration, () =>
        {
            element.HitWithbullet(elementID);

            ElementCreator.UnFreeze();
        });

        BulletLauncher.LaunchBullet(hitPoint, hitTween, elementID != element.Config.ElementTypeID && elementID != -1, elementID != -1);
        if (Settings.UseColorForBullet)
            BulletLauncher.ShowNextBullet(ElementCreator.GetRandomBulletConfig());
        else
            BulletLauncher.ShowNextBullet(null);
    }

    private void OnItemClicked(CylinderElement element, Vector3 hitPoint)
    {
        if (_gameDatas.ShotsLeft == 0)
        {
            LevelManager.Instance.LoadCurrentLevel();
            return;
        }

        if (element.FloorHit)
            return;

        FireBullet(element, hitPoint);

        if (_freezeEndTween != null)
            _freezeEndTween.Kill();
        _freezeEndTween = DOVirtual.DelayedCall(FreezeInitialTimer, () =>
        {
            Freeze();
        });
    }

    private void Freeze()
    {
        if (!ElementCreator.Freeze())
        {
            _freezeEndTween = DOVirtual.DelayedCall(FreezeRepeatTimer, () =>
            {
                Freeze();
            });
        }
    }

    private void OnEmptyClicked()
    {
        if (_gameDatas.ShotsLeft == 0)
            LevelManager.Instance.LoadCurrentLevel();
    }
}
