﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClickHandler : MonoBehaviour
{
    public PlayerClickedEvent PlayerClickedEvent;
    // ItemClickedEvent is triggered when the player click on an item
    // Param1 : the item clicked
    // Param2 : the collision point
    public ItemClickedEvent ItemClickedEvent;
    // EmptyClickedEvent is triggered when the player click on nothing
    public EmptyClickedEvent EmptyClickedEvent;
    public Camera Camera;
    public LayerMask LayersToTarget;
    
    private void Awake()
    {
        PlayerClickedEvent.Event += OnPlayerClicked;
    }

    private void OnDestroy()
    {
        PlayerClickedEvent.Event -= OnPlayerClicked;
    }

    public void OnPlayerClicked(Vector2 screenPosition)
    {
        Ray ray = Camera.ScreenPointToRay(screenPosition);

        // This worked, but we need more precision because our cylinders have a box trigger collider touching each other
        /*        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayersToTarget.value))
        {
            CylinderElement element = hit.collider.GetComponent<CylinderElement>();
            if (element != null)
            {
                ItemClickedEvent.Trigger(element);
                return;
            }
        }*/

        // We will use RaycastAll for more precision, and find the nearest element hit from the first collision point.
        RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity, LayersToTarget.value);
        if (hits.Length > 0)
        {
            Vector3 originPoint = ray.origin;

            // Find the first collision point
            Vector3 firstHitPoint = ray.origin;
            float closestHitDist = Mathf.Infinity;
            foreach (RaycastHit hit in hits)
            {
                CylinderElement element = hit.collider.GetComponent<CylinderElement>();
                if (element != null)
                {
                    float hitDist = (hit.point - originPoint).sqrMagnitude;
                    if (hitDist < closestHitDist)
                    {
                        firstHitPoint = hit.point;
                        closestHitDist = hitDist;
                    }
                }
            }

            // Find the nearest hit element
            CylinderElement closest = null;
            float dist = Mathf.Infinity;
            foreach (RaycastHit hit in hits)
            {
                CylinderElement element = hit.collider.GetComponent<CylinderElement>();
                if (element != null)
                {
                    float curDist = (element.transform.position - firstHitPoint).sqrMagnitude;
                    if (curDist < dist)
                    {
                        closest = element;
                        dist = curDist;
                    }
                }
            }

            if (closest != null)
            {
                ItemClickedEvent.Trigger(closest, firstHitPoint);
                return;
            }
        }
        EmptyClickedEvent.Trigger();
    }
}
