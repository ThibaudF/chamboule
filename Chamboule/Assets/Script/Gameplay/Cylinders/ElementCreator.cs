﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementCreator : MonoBehaviour
{
    public const float MAX_DEGREE = 360f;

    public FloorDisappearEvent OnFloorDisappearEvent;
    public GameSettings Settings;
    public Material DefaultMaterial;
    public LayerMask AvailableLayerMask;
    public int AvailableLayer{ get{ return Utils.layermask_to_layer(AvailableLayerMask); } }
    public LayerMask DisabledLayerMask;
    public int DisabledLayer{ get{ return Utils.layermask_to_layer(DisabledLayerMask); } }

    private List<List<CylinderElement>> _currentElements = new List<List<CylinderElement>>();
    private List<CylinderElement> _oldElements = new List<CylinderElement>();
    private List<CylinderElement> _elementsReferences = new List<CylinderElement>();
    private List<FixedJoint> _currentJoints = new List<FixedJoint>();

    private void Awake()
    {
        OnFloorDisappearEvent.Event += RemoveRow;
    }

    private void OnDestroy()
    {
        OnFloorDisappearEvent.Event -= RemoveRow;
    }

    public void Init(Vector3 origin, float baseY, LevelInfos levelInfos)
    {
        _elementsReferences = Utils.SelectRandomElements(Settings.DefaultCylinders, levelInfos.ColorQuantity, true);
        _currentElements = InstantiateItems(origin, levelInfos.FloorsQuantity, Settings.FloorHeight, baseY + Settings.FloorHeight, Settings.ItemQuantity);
        JointFloors();
    }

    public void Init(Vector3 origin, float baseY, SingleLevelDesign levelDesign)
    {
        _elementsReferences = Utils.SelectRandomElements(Settings.DefaultCylinders, levelDesign.LevelInfos.ColorQuantity, true);
        _currentElements = InstantiateItems(origin, levelDesign, Settings.FloorHeight, baseY + Settings.FloorHeight);
        JointFloors();
    }

    // For more stability, floor 0 is double joined with the first immobile floor, and floor 1 is simple joined with floor 0
    private void JointFloors()
    {
        foreach (FixedJoint joint in _currentJoints)
        {
            joint.connectedBody.gameObject.SetActive(false);
            Destroy(joint);
        }
        _currentJoints.Clear();
        int originFloor = _currentElements.Count - Settings.AccessibleFloors - 1;
        if (originFloor < 0)
            return;

        List<CylinderElement> lBottom = _currentElements[originFloor];
        List<CylinderElement> l = _currentElements[originFloor + 1];
        List<CylinderElement> lUpper = _currentElements[originFloor + 2];
        for (int i = 0; i < l.Count; i++)
        {
            CylinderElement bottom = lBottom[i];
            CylinderElement left = null;
            if (i == 0)
                left = l[l.Count - 1];
            else
                left = l[i - 1];
            CylinderElement right = l[i];
            CylinderElement top = lUpper[i];

            _currentJoints.Add(addJoint(left, bottom));
            _currentJoints.Add(addJoint(right, bottom));
            _currentJoints.Add(addJoint(top, right));
        }
    }

    public void RemoveRow()
    {
        if (_currentElements.Count > 0)
        {
            // Remove last row
            _oldElements.AddRange(_currentElements[_currentElements.Count - 1]);
            foreach (CylinderElement element in _currentElements[_currentElements.Count - 1])
            {
                element.RigidBody.isKinematic = false;
            }
            _currentElements.RemoveAt(_currentElements.Count - 1);

            // Activate old floor -1
            if (_currentElements.Count >= Settings.AccessibleFloors)
            {
                foreach (CylinderElement element in _currentElements[_currentElements.Count - Settings.AccessibleFloors])
                {
                    element.Activate(AvailableLayer);
                }
            }

            // Deactivate old floor -2, but enable colliders to be the current bottom floor
            if (_currentElements.Count >= Settings.AccessibleFloors + 1)
            {
                foreach (CylinderElement element in _currentElements[_currentElements.Count - Settings.AccessibleFloors - 1])
                {
                    element.Deactivate(DefaultMaterial, DisabledLayer, true);
                }
            }


            // Hide old elements on the floor
            float fallenObjectYReference = 0f;
            if (_currentElements.Count > Settings.AccessibleFloors + 5)
            {
                fallenObjectYReference = _currentElements[2][0].transform.position.y;
                foreach (CylinderElement element in _oldElements)
                {
                    if (element.gameObject.activeSelf && element.transform.position.y <= fallenObjectYReference)
                    {
                        element.gameObject.SetActive(false);
                    }
                }
            }

            JointFloors();
        }
    }

    // 1 FixedUpdate that update everything is better than 1 fixedUpdate on everything.
    public void FixedUpdate()
    {
        for (int i = 0; i < _currentJoints.Count; i++)
        {
            if (!_currentJoints[i].connectedBody.gameObject.activeSelf || !_currentJoints[i].connectedBody.gameObject.activeInHierarchy)
            {
                Destroy(_currentJoints[i]);
                _currentJoints.RemoveAt(i--);
            }
        }
        for (int i = _currentElements.Count - 1; i >= 0 && i >= _currentElements.Count - 1 - Settings.AccessibleFloors; i--)
        {
            List<CylinderElement> l = _currentElements[i];
            foreach (CylinderElement item in l)
            {
                if (item.RigidBody.isKinematic == false)
                    item.RefreshCollision();
            }
        }
    }

    public void UnFreeze()
    {
        int floorsAlive = Settings.AccessibleFloors;
        int min = Mathf.Max(0, _currentElements.Count - floorsAlive);
        for (int i = min; i < _currentElements.Count; i++)
        {
            foreach (CylinderElement element in _currentElements[i])
            {
                element.RigidBody.isKinematic = false;
            }
        }
    }

    public bool Freeze()
    {
        int min = Mathf.Max(0, _currentElements.Count - Settings.AccessibleFloors);
        bool canFreeze = true;
        for (int i = min; i < _currentElements.Count; i++)
        {
            foreach (CylinderElement element in _currentElements[i])
            {
                element.ClearInactiveTouchingCollider();
                element.RefreshCollision();
                if (element.InCrutialMove)
                    canFreeze = false;
            }
        }
        if (canFreeze)
        {
            for (int i = min; i < _currentElements.Count; i++)
            {
                foreach (CylinderElement element in _currentElements[i])
                {
                    if (element.CanBeFrozen)
                        element.RigidBody.isKinematic = true;
                }
            }
        }

        return canFreeze;
    }

    public List<List<CylinderElement>> InstantiateItems(Vector3 origin, int floors, float height, float initialHeight, int itemQuantity)
    {
        List<List<CylinderElement>> res = new List<List<CylinderElement>>();
        float currentHeight = initialHeight;
        float currentOffset = 0f;
        for (int i = 0; i < floors; i++)
        {
            List<CylinderElement> currentFloorObjects = new List<CylinderElement>();
            res.Add(currentFloorObjects);

            //TODO OPTIMIZATION : one circle only to rotate, and randomize only the colors + pooling
            List<Vector3> circleItems = CreateItemsCircleAround(origin, itemQuantity, Settings.DistanceFromCenter, currentHeight, currentOffset);
            float distBetweenTwoItems = (circleItems[0] - circleItems[1]).magnitude;
            foreach (Vector3 item in circleItems)
            {
                int rand = UnityEngine.Random.Range(0, _elementsReferences.Count);
                CylinderElement newCylinder = Instantiate<CylinderElement>(_elementsReferences[rand], this.transform);
                newCylinder.transform.position = item;
                newCylinder.transform.localScale = new Vector3(distBetweenTwoItems, height, distBetweenTwoItems);
                newCylinder.Init(Settings.ExplosionStrength);
                if (i < floors - Settings.AccessibleFloors)
                {
                    newCylinder.Deactivate(DefaultMaterial, DisabledLayer, i == floors - Settings.AccessibleFloors - 1);
                }
                else
                {
                    newCylinder.Activate(AvailableLayer);
                }
                newCylinder.RigidBody.isKinematic = true;
                currentFloorObjects.Add(newCylinder);
            }

            currentOffset += Settings.OffsetPerFloor;
            currentHeight += height * 2; // 1 cylinder scale = 2 position unit
        }

        /* Joint test for stability, not worth it
        for (int i = 0; i < res.Count - 1; i++)
        {
            List<CylinderElement> l = res[i];
            List<CylinderElement> nextL = res[i + 1];
            for (int j = 0; j < l.Count; j++)
            {
                CylinderElement topLeft;
                if (j == 0)
                    topLeft = nextL[nextL.Count - 1];
                else
                    topLeft = nextL[j - 1];
                CylinderElement topRIght = nextL[j];

                CylinderElement elem = l[j];

                addJoint(topLeft, elem);
                addJoint(topRIght, elem);
            }
        }*/

        return res;
    }

    public List<List<CylinderElement>> InstantiateItems(Vector3 origin, SingleLevelDesign design, float height, float initialHeight)
    {
        List<List<CylinderElement>> res = new List<List<CylinderElement>>();
        float currentHeight = initialHeight;
        float currentOffset = 0f;
        List<LineOfLevel> levelItems = design.Items;
        float floors = levelItems.Count;
        for (int i = 0; i < floors; i++)
        {
            List<int> line = levelItems[i].Line;
            List<CylinderElement> currentFloorObjects = new List<CylinderElement>();
            res.Add(currentFloorObjects);

            //TODO OPTIMIZATION : one circle only to rotate, and randomize only the colors + pooling
            List<Vector3> circleItems = CreateItemsCircleAround(origin, line.Count, Settings.DistanceFromCenter, currentHeight, currentOffset);
            float distBetweenTwoItems = (circleItems[0] - circleItems[1]).magnitude;
            for (int j = 0; j < circleItems.Count; j++)
            {
                Vector3 pos = circleItems[j];
                int rand = UnityEngine.Random.Range(0, _elementsReferences.Count);
                CylinderElement newCylinder = Instantiate<CylinderElement>(_elementsReferences[line[j]], this.transform);
                newCylinder.transform.position = pos;
                newCylinder.transform.localScale = new Vector3(distBetweenTwoItems, height, distBetweenTwoItems);
                newCylinder.Init(Settings.ExplosionStrength);
                if (i < floors - Settings.AccessibleFloors)
                {
                    newCylinder.Deactivate(DefaultMaterial, DisabledLayer, i == floors - Settings.AccessibleFloors - 1);
                }
                else
                {
                    newCylinder.Activate(AvailableLayer);
                }
                newCylinder.RigidBody.isKinematic = true;
                currentFloorObjects.Add(newCylinder);
            }

            currentOffset += Settings.OffsetPerFloor % 1;
            currentHeight += height * 2; // 1 cylinder scale = 2 position unit
        }

        return res;
    }

    private static FixedJoint addJoint(CylinderElement fromObj, CylinderElement toObj)
    {
        var joint = fromObj.gameObject.AddComponent<FixedJoint>();
        joint.connectedBody = toObj.KinematicBody;
        toObj.KinematicBody.gameObject.SetActive(true);
        fromObj.Joints.Add(joint);
        return joint;
    }

    private static List<Vector3> CreateItemsCircleAround(Vector3 origin, int quantity, float distanceFromOrigin, float heightToAdd, float initialOffset = 0f)
    {
        float itemsHeight = origin.y + heightToAdd;
        float degreeOffset = MAX_DEGREE / quantity;
        float radOffset = Mathf.Deg2Rad * degreeOffset;
        double cosTheta = Math.Cos(radOffset);
        double sinTheta = Math.Sin(radOffset);
        List<Vector3> circleItemsPositions = new List<Vector3>();
        Vector2 origin2D = new Vector2(origin.x, origin.z);
        Vector2 initialPosition = new Vector2(origin2D.x + distanceFromOrigin, origin2D.y);
        if (initialOffset != 0)
        {
            float initialRadOffset = Mathf.Deg2Rad * degreeOffset * initialOffset;
            double initialCosTheta = Math.Cos(initialRadOffset);
            double initialSinTheta = Math.Sin(initialRadOffset);
            initialPosition = Utils.RotatePoint(initialPosition, origin2D, initialRadOffset, false, initialCosTheta, initialSinTheta);
        }
        Vector2 currentPosition = initialPosition;
        circleItemsPositions.Add(new Vector3(currentPosition.x, itemsHeight, currentPosition.y));
        for (int i = 1; i < quantity; i++)
        {
            currentPosition = Utils.RotatePoint(currentPosition, origin2D, radOffset, false, cosTheta, sinTheta);
            circleItemsPositions.Add(new Vector3(currentPosition.x, itemsHeight, currentPosition.y));
        }

        return circleItemsPositions;
    }

    public CylinderElementConfig GetRandomBulletConfig()
    {
        int rand = UnityEngine.Random.Range(0, _elementsReferences.Count);
        return _elementsReferences[rand].Config;
    }
}
