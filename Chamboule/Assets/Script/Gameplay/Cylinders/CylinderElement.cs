﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CylinderElement : MonoBehaviour
{
    public CylinderElementConfig Config;
    public MeshRenderer Mesh;
    public Rigidbody RigidBody;
    public Rigidbody KinematicBody;
    public List<Collider> Colliders;
    public List<FixedJoint> Joints = new List<FixedJoint>();


    private float _explosionStrength;
    private bool _hasFallen = false;
    private bool _destroyed = false;
    private bool _isActive = true;
    private bool _FloorHit = false;
    public bool FloorHit { get { return _FloorHit; } }

    private HashSet<CylinderElement> _touchingCollider = new HashSet<CylinderElement>();

    public bool CanBeFrozen
    {
        get
        {
            return !_hasFallen && !_destroyed;
        }
    }

    public bool InCrutialMove
    {
        get
        {
            return !_hasFallen && !_destroyed && RigidBody.velocity.sqrMagnitude >= 0.5f;
        }
    }

    public bool IsActive
    {
        get
        {
            return _isActive && !_destroyed;
        }
    }

    public void Init(float explosionStrength)
    {
        _hasFallen = false;
        _destroyed = false;
        _FloorHit = false;
        _explosionStrength = explosionStrength;
        KinematicBody.gameObject.SetActive(false);
    }

    public void ClearInactiveTouchingCollider()
    {
        List<CylinderElement> toRemove = new List<CylinderElement>();
        foreach (CylinderElement elem in _touchingCollider)
        {
            if (elem._destroyed || elem.FloorHit)
            {
                toRemove.Add(elem);
            }
        }
        foreach (CylinderElement elem in toRemove)
        {
            _touchingCollider.Remove(elem);
        }
    }

    public void RefreshCollision()
    {
        if (_touchingCollider.Count == 0)
            _hasFallen = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        CylinderElement element = other.GetComponent<CylinderElement>();
        if (element != null)
        {
            _touchingCollider.Add(element);
        }
    }


    private void OnTriggerExit(Collider other)
    {
        CylinderElement element = other.GetComponent<CylinderElement>();
        if (element != null)
        {
            _touchingCollider.Remove(element);
        }
    }

    public void HitWithbullet(int elementTypeID)
    {
        if (!IsActive)
            return;

        if (elementTypeID == -1)
            elementTypeID = Config.ElementTypeID;

        if (elementTypeID == Config.ElementTypeID)
        {
            _isActive = false;

            List<CylinderElement> elementsToTrigger = new List<CylinderElement>(_touchingCollider);
            DOVirtual.DelayedCall(0.05f, () =>
            {
                if (this != null && gameObject != null)
                {
                    _destroyed = true;
                    gameObject.SetActive(false);
                    SpreadHitToNeighbour(elementsToTrigger, elementTypeID);
                }
            });
        }
    }

    private void SpreadHitToNeighbour(List<CylinderElement> elementsToTrigger, int elementTypeID)
    {
        foreach (CylinderElement element in elementsToTrigger)
        {
            if (element != null)
                element.HitWithbullet(elementTypeID);

            if (element.transform.position.y >= transform.position.y)
            {
                Vector3 dir = (element.transform.position - transform.position).normalized;
                dir *= _explosionStrength;
                element.RigidBody.AddForce(dir);
            }
        }
    }

    public void Activate(int layer)
    {
        foreach (Collider collider in Colliders)
        {
            collider.enabled = true;
        }
        RigidBody.isKinematic = false;
        Mesh.material = Config.AvailableMaterial;
        gameObject.layer = layer;
        _isActive = true;
        RigidBody.constraints = RigidbodyConstraints.None;
    }

    public void Deactivate(Material deactivatedMaterial, int layer, bool enableCollider = false)
    {
        foreach (Collider collider in Colliders)
        {
            collider.enabled = enableCollider;
        }
        RigidBody.isKinematic = !enableCollider;
        if (enableCollider)
            RigidBody.constraints = RigidbodyConstraints.FreezeAll;
        Mesh.material = deactivatedMaterial;
        gameObject.layer = layer;
        _isActive = false;
    }

    public void TriggerFloorHit()
    {
        _FloorHit = true;
    }
}
