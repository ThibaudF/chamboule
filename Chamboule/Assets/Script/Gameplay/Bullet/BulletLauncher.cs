﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class BulletLauncher : MonoBehaviour
{
    [NonSerialized]
    public CylinderElementConfig NextBulletConfig;
    public BulletProjectile FakeBullet;
    public int BulletPathDuration;

    public void ShowNextBullet(CylinderElementConfig config)
    {
        FakeBullet.gameObject.SetActive(false);
        if (config != null)
        {
            FakeBullet.SetMaterial(config.AvailableMaterial);
            NextBulletConfig = config;
        }
        FakeBullet.gameObject.SetActive(true);
    }

    public void LaunchBullet(Vector3 targetPosition, Tween hitTween, bool bounceAtEnd, bool bounceOnTriggerEnter)
    {
        BulletProjectile bullet = Instantiate<BulletProjectile>(FakeBullet);
        bullet.transform.position = FakeBullet.transform.position;
        Tween moveTween = bullet.transform.DOMove(targetPosition, BulletPathDuration).OnComplete(() =>
        {
            if (bullet != null)
            {
                bullet.EndPath(bounceAtEnd);
            }
        });
        moveTween.timeScale = 1 / Time.timeScale;
        bullet.PrepareForLaunch(hitTween, moveTween, bounceOnTriggerEnter);
    }

}
