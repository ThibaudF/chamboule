﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionChecker : MonoBehaviour
{
    public BulletProjectile Projectile;

    private void OnTriggerEnter(Collider other)
    {
        Projectile.AnnounceTriggerEnter(other);
    }
}
