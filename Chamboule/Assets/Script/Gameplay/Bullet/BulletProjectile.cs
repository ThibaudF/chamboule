﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    public ScreenShakeConfig ShakeEventConfig;
    public MeshRenderer Visual;
    public Animator BounceAnimator;
    public Rigidbody Body;
    public Collider Collider;
    public float BounceBackPower;
    public float BounceBackHigh;
    public float TriggerEnterAcceptanceOffsetDuration;
    public float BumpRadiusMin;
    public float BumpRadiusMax;
    public float AutoDestroyTimer;

    private Tween _hitTween;
    private Tween _moveTween;
    private Vector3 _initialPosition;
    private bool _bounceOnTriggerEnter;

    private void OnDestroy()
    {
        KillTweens();
    }
    
    private void KillTweens()
    {
        if (_hitTween != null)
            _hitTween.Kill();
        if (_moveTween != null)
            _moveTween.Kill();
    }

    public void PrepareForLaunch(Tween hitTween, Tween moveTween, bool bounceOnTriggerEnter)
    {
        BounceAnimator.enabled = true;
        Collider.enabled = true;
        _hitTween = hitTween;
        _moveTween = moveTween;
        _initialPosition = transform.position;
        _bounceOnTriggerEnter = bounceOnTriggerEnter;
    }

    public void AnnounceTriggerEnter(Collider other)
    {
        if (BounceAnimator.enabled &&
            (_moveTween.Elapsed() / _moveTween.Duration() <= TriggerEnterAcceptanceOffsetDuration))
        {
            CylinderElement element = other.GetComponent<CylinderElement>();
            if (element != null)
            {
                HandleCollisionWithElement(element);
            }
        }
    }

    private void HandleCollisionWithElement(CylinderElement element)
    {
        KillTweens();

        if (!_bounceOnTriggerEnter)
            element.HitWithbullet(-1);

        EndPath(_bounceOnTriggerEnter);
    }

    public void BounceBack()
    {
        BounceAnimator.enabled = false;
        Collider.isTrigger = false;
        Body.useGravity = true;

        float radAngle;
        if (UnityEngine.Random.Range(0, 2) == 1)
            radAngle = UnityEngine.Random.Range(-BumpRadiusMax, -BumpRadiusMin) * Mathf.Deg2Rad;
        else
            radAngle = UnityEngine.Random.Range(BumpRadiusMax, BumpRadiusMin) * Mathf.Deg2Rad;

        Vector2 dest2D = Utils.RotatePoint(new Vector2(_initialPosition.x, _initialPosition.z), new Vector2(transform.position.x, transform.position.z), radAngle);
        Vector3 dir = (new Vector3(dest2D.x, transform.position.y, dest2D.y) - transform.position).normalized;
        dir *= BounceBackPower;
        dir.y = BounceBackHigh;
        Body.AddForce(dir);

        DOVirtual.DelayedCall(AutoDestroyTimer, () =>
        {
            if (this != null)
                Destroy(gameObject);
        });
    }

    public void EndPath(bool shouldBounce)
    {
        if (shouldBounce)
        {
            BounceBack();
        }
        else
        {
            _hitTween = null;
            _moveTween = null;
            ShakeEventConfig.Trigger();
            Destroy(gameObject);
        }
    }

    public void SetMaterial(Material mat)
    {
        Visual.material = mat;
    }
}
